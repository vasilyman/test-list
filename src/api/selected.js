import History from '@/api/history';

class Selected {
  constructor(data = {}) {
    this.id = data.parentId;
    this.name = data.name;
  }

  items = [];

  static add(item = {}) {
    this.items.push(new Selected(item));
    History.add(item);
  }

  static remove(remove = {}) {
    this.items = this.items.filter((item) => item.id !== remove.id);
    History.add(remove, true);
  }
}

export default new Selected();
