export default class History {
  constructor(data = {}) {
    this.id = data.parentId;
    this.name = data.name;
    this.operation = data.operation;
    this.createdAt = new Date();
  }

  items = [];

  static add(item = {}, remove = false) {
    this.items.push(new History({ ...item, operation: remove ? 'remove' : 'add' }));
  }
}
