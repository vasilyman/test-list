import http from '@/api';

export default class User {
  constructor(data = {}) {
    this.id = data.parentId ? `child-${data.id}` : data.id;
    this.name = data.name;
    this.parentId = data.parentId;
  }

  static async getList(filter = {}) {
    const url = 'parents';
    const data = filter;
    const res = await http({
      method: 'get',
      url,
      data,
    });
    return res.data?.reduce((acc, cur) => {
      const result = [...acc];
      result.push(new User(cur), ...(cur.items?.map((item = {}) => new User(item)) || []));
      return result;
    }, []);
  }
}
