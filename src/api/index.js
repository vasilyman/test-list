import axios from 'axios';

const http = axios.create({
  baseURL: 'https://61642af5b55edc00175c1dd8.mockapi.io/api/v1',
});

export default http;
