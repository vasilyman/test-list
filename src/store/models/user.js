export default class User {
  constructor(data = {}) {
    this.id = data.parentId ? `child-${data.id}` : data.id;
    this.name = data.name;
    this.parentId = data.parentId;
  }
}
