export default class History {
  constructor(data = {}) {
    this.id = data.id;
    this.name = data.name;
    this.operation = data.remove ? 'remove' : 'add';
    this.createdAt = new Date();
  }
}
