export default class Selected {
  constructor(data = {}) {
    this.id = data.id;
    this.name = data.name;
  }
}
