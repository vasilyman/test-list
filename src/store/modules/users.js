import http from '@/api';
import User from '@/store/models/user';
import Selected from '@/store/models/selected';
import History from '@/store/models/history';

export default ({
  namespaced: true,
  state: {
    users: [],
    selected: [],
    history: [],
  },
  mutations: {
    SET_USERS(state, data) {
      state.users = data.map((item) => new User(item));
    },
    ADD_SELECTED(state, data) {
      state.selected.push(new Selected(data));
    },
    REMOVE_SELECTED(state, data) {
      state.selected = state.selected.filter((user) => user.id !== data.id);
    },
    ADD_HISTORY(state, data) {
      state.history.push(new History(data));
    },
  },
  actions: {
    async getUsers({ commit }, filter) {
      const url = 'parents';
      const data = filter;
      const res = await http({
        method: 'get',
        url,
        data,
      });
      const users = res.data?.reduce((acc, cur) => {
        const result = [...acc];
        result.push(cur, ...(cur.items || []));
        return result;
      }, []);
      commit('SET_USERS', users);
    },
    addItem({ state, commit, dispatch }, item = {}) {
      if (state.selected.some((user) => user.id === item.id)) return;
      dispatch('addHistory', { item });
      commit('ADD_SELECTED', item);
    },
    removeItem({ commit, dispatch }, item = {}) {
      dispatch('addHistory', { item, remove: true });
      commit('REMOVE_SELECTED', item);
    },
    addHistory({ commit }, { item, remove }) {
      commit('ADD_HISTORY', { ...item, remove });
    },
  },
  getters: {
    getUnselected: (state) => state.users
      .filter((user) => !state.selected.some((item) => item.id === user.id)),
  },
});
