import Vue from 'vue';
import Vuex from 'vuex';
import User from '@/store/modules/users';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    menu: [
      {
        to: { name: 'index' },
        text: 'Home',
      },
      {
        to: { name: 'history' },
        text: 'History',
      },
      {
        to: { name: 'history', query: { f: 'add' } },
        text: 'History adds',
      },
      {
        to: { name: 'history', query: { f: 'remove' } },
        text: 'History dels',
      },
    ],
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    User,
  },
});
